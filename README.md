# Headway.Global Task

This is a test task for Headway.Global.

## Initialized NodeJS project from package.json.

```javascript
{
  "name": "docker_web_app",
  "version": "1.0.0",
  "description": "Node.js on Docker",
  "author": "Aleksandr Onyshchenko <alex.dev936@gmail.com>",
  "main": "server.js",
  "scripts": {
    "start": "node server.js"
  },
  "dependencies": {
    "express": "^4.16.1"
  }
}
```
## Created simple echo http server.

```javascript
const express = require('express');

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();
app.get('/', (req, res) => {
  res.send('Hello World');
});

app.listen(PORT, HOST);
```

## Created Dockerfile for the app.

```
FROM node:12

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8080
CMD [ "node", "server.js" ]
```

## Created pipeline for GitLab that builds and pushed to app image to GitLab registry.

```
image: docker:latest
services:
  - docker:dind

stages:
  - build

variables:
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

build:
  stage: build
  script:
    - docker build --pull -t $CONTAINER_TEST_IMAGE .
    - docker push $CONTAINER_TEST_IMAGE
  only:
    - master
```

## Created **nginx** configuration **nginx.conf** to serve https and proxy to the running docker container on a server.

```
server {
    listen 80;
    return 301 https://$host$request_uri;
}

server {
    listen 443;

    server_name node.example.com;
    
    ssl_certificate           /etc/nginx/cert.crt;
    ssl_certificate_key       /etc/nginx/cert.key;

    ssl on;
    ssl_session_cache  builtin:1000  shared:SSL:10m;
    ssl_protocols  TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers HIGH:!aNULL:!eNULL:!EXPORT:!CAMELLIA:!DES:!MD5:!PSK:!RC4;
    ssl_prefer_server_ciphers on;

    access_log            /var/log/nginx/docker_web_app.access.log;

    location / {
      proxy_set_header        Host $host;
      proxy_set_header        X-Real-IP $remote_addr;
      proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header        X-Forwarded-Proto $scheme;
       # We assume that docker container is running and listening on port 8080. (docker run -p 8080 -d registry.gitlab.com/alex-onyshchenko/headway.global-task:master)
      proxy_pass          http://localhost:8080;
    }
  }
```

